//
//  UICustom.swift
//  TwitSplit
//
//  Created by TamTran on 2/26/19.
//  Copyright © 2019 TamTran. All rights reserved.
//

import UIKit

extension UIButton {
    
    
    func customizeUI() {
        
        self.setTitleColor(UIColor.white, for: .normal)
        self.backgroundColor = UIColor(red: 142.0/255.0, green: 68.0/255.0, blue: 173.0/255.0, alpha: 1.0)
        self.layer.cornerRadius = 25
        
    }
    
    
}

extension UITextView {
    
    func customizeUI() {
        
        
        self.layer.cornerRadius = 10
        
        self.layer.shadowColor = UIColor.gray.cgColor
        
        self.layer.borderColor = UIColor.gray.cgColor
        
        self.layer.borderWidth = 1
        
        
    }

}

extension UILabel {
    
    func customizeUI() {
        
        
        self.layer.cornerRadius = 10
        
        self.layer.shadowColor = UIColor.gray.cgColor
        
        self.layer.borderColor = UIColor.gray.cgColor
        
        self.layer.borderWidth = 1
        
        
    }

    
}
