//
//  TweeterViewController.swift
//  TwitSplit
//
//  Created by TamTran on 2/26/19.
//  Copyright © 2019 TamTran. All rights reserved.
//

import UIKit

class TweeterViewController: UIViewController {

    
    @IBOutlet weak var textView: UITextView!
    
    @IBOutlet weak var buttonSend: UIButton!

    @IBOutlet weak var tvResult: UITextView!
    
    @IBOutlet weak var lblCharacters: UILabel!
    
    var numberOfCharacters : Int!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.initUI()
        
        self.initData()
    }
    
    
    func initUI() {
        
        self.textView.text = ""
        
        self.buttonSend.customizeUI()
        
        self.textView.customizeUI()
        
        self.tvResult.customizeUI()
        
        self.tvResult.text = ""
        
        self.tvResult.isEditable = false
        
        self.textView.delegate = self
        
    }
    
    func initData() {
        
        self.calculateNumberOfCharacters(self.textView.text.count)
        
        self.showNumberOfCharacters(self.numberOfCharacters)
   
    }
    
    func calculateNumberOfCharacters(_ numberOfText:Int) {
        
        self.numberOfCharacters = numberOfText
        
        let numberOfMessages = Int((Float(self.numberOfCharacters) / Float(LIMIT_OF_CHARATER - NUMBER_OF_INDICATOR_CHARACTERS)).rounded(.up))
        
        if self.numberOfCharacters > LIMIT_OF_CHARATER {
            
            self.numberOfCharacters = self.numberOfCharacters + NUMBER_OF_INDICATOR_CHARACTERS * numberOfMessages
            
        } else {
            
            
            
        }
    }
    
    func showNumberOfCharacters(_ number:Int) {
        
        self.lblCharacters.text = String(number)
        
    }
    
    func tweetMessage(_ message:String?) {
        
      
        if let msg = message {
        
        let result = self.splitTweetMessage(msg)
        
        self.showTweetMessage(result)
            
        } else {
            
        self.showTweetMessage("")
            
        }
        
    }
    
    func splitTweetMessage(_ message:String) -> String{
        
        var arrayTweeterText = NSMutableArray()
        
        arrayTweeterText = message.splitMessage()
        
         var result = ""
        
        if arrayTweeterText.count == 0 {

            return result
        } else if arrayTweeterText.count == 1 {
        
        result = arrayTweeterText[0] as! String
         
            return result
        } else if arrayTweeterText.count > 1 {
        
        var text = ""
        
        let length = arrayTweeterText.count
        
            for (index,item) in arrayTweeterText.enumerated() {
                
                text = "\(index + 1)/\(length) " + (item as! String)
                
                result  += text + "\n"
                
            }
        
        }
        
        return result
    }
    
    func showTweetMessage(_ result:String) {
        
        self.tvResult.text = result
        
    }
    

    @IBAction func didTouchSendMessage(_ sender: Any) {
        
        self.view.endEditing(true)
        
        self.tweetMessage(self.textView.text)
        
        
    }

}

extension TweeterViewController : UITextViewDelegate {
    
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        
        let numberOfCharacters = self.textView.text.count + (text.count - range.length)
        
        self.calculateNumberOfCharacters(numberOfCharacters)
        
        self.showNumberOfCharacters(self.numberOfCharacters)
        
        return true
        
    }
    
}
