//
//  ViewController.swift
//  TwitSplit
//
//  Created by TamTran on 2/26/19.
//  Copyright © 2019 TamTran. All rights reserved.
//

import UIKit

class HomeViewController: UIViewController {

    @IBOutlet weak var buttonTweeter: UIButton!
   
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        self.initUI()
        
    }
    
    
    func initUI() {
        
        self.buttonTweeter.customizeUI()
        
    }
    
    
    @IBAction func didTouchTweeterMessage(_ sender: Any) {
    
        let tweeterViewController = self.storyboard?.instantiateViewController(withIdentifier: TWEETER_VIEW_CONTROLLER)
        
        self.navigationController?.pushViewController(tweeterViewController ?? TweeterViewController(), animated: true)
        
    }
    


}

