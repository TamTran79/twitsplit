//
//  Util.swift
//  TwitSplit
//
//  Created by TamTran on 2/26/19.
//  Copyright © 2019 TamTran. All rights reserved.
//

import Foundation
import UIKit

extension String {
    
    var length: Int {
        return count
    }
    
    subscript (i: Int) -> String {
        return self[i ..< i + 1]
    }
    
    func substring(fromIndex: Int) -> String {
        return self[min(fromIndex, length) ..< length]
    }
    
    func substring(toIndex: Int) -> String {
        return self[0 ..< max(0, toIndex)]
    }
    
    subscript (r: Range<Int>) -> String {
        let range = Range(uncheckedBounds: (lower: max(0, min(length, r.lowerBound)),
                                            upper: min(length, max(0, r.upperBound))))
        let start = index(startIndex, offsetBy: range.lowerBound)
        let end = index(start, offsetBy: range.upperBound - range.lowerBound)
        return String(self[start ..< end])
    }
    
    func splitMessage() -> NSMutableArray {
        
        let arrayMessage = NSMutableArray()
        
        if self.count <= LIMIT_OF_CHARATER
        {
            arrayMessage.add(self)
            
            return arrayMessage
        } else {
            
            var position = 0
            
            var subText = ""
            
            let numberOfMessages = Float( self.count / (LIMIT_OF_CHARATER - NUMBER_OF_INDICATOR_CHARACTERS)).rounded(.up)

            let numberOfMoreIndicatorCharacters = Int(numberOfMessages/10)
            
            let length = LIMIT_OF_CHARATER - ( NUMBER_OF_INDICATOR_CHARACTERS + numberOfMoreIndicatorCharacters)
            
            repeat {
                
                if position + length - 1 >= self.count - 1 {
                    
                    subText = self[position..<(self.count)]
                    
                    arrayMessage.add(subText)
                    
                    break
                    
                }
                
                if self[position + length - 1] == SPLIT_CHARACTER {
                    
                    subText = self[position..<(position + length)]
                    
                    arrayMessage.add(subText)
                    
                    position = position + length
                    
                } else if self[position + length - 1] != SPLIT_CHARACTER {
                    
                    if self[position + length ] == SPLIT_CHARACTER {
                        
                        subText = self[position..<(position + length + 1)]
                        
                        arrayMessage.add(subText)
                        
                        position = position + length + 1
                        
                        
                    } else {
                        
                        var posSpace = position + length - 1// scan from character before position = (position + length)
                        
                        repeat {
                            
                            posSpace = posSpace - 1
                            
                        } while self[posSpace] != SPLIT_CHARACTER
                        
                        subText = self[position..<posSpace]
                        
                        arrayMessage.add(subText)
                        
                        position = posSpace + 1
                        
                    }
                    
                }
                
                
            } while position < self.count
            
        }
        
        return arrayMessage
        
    }
    
}
