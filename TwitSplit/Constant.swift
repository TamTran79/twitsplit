//
//  Constant.swift
//  TwitSplit
//
//  Created by TamTran on 2/26/19.
//  Copyright © 2019 TamTran. All rights reserved.
//



let HOME_VIEW_CONTROLLER = "HomeViewController"

let TWEETER_VIEW_CONTROLLER = "TweeterViewController"

let LIMIT_OF_CHARATER = 50

let NUMBER_OF_INDICATOR_CHARACTERS = 4

let SPLIT_CHARACTER = " "
